\section{Introduction}
\label{sec:introduction}

Emerging data analytics applications aim at processing unprecedented amount of
data, posing novel challenges to both industry and the scientific community.
When working with big data, scalability is more than a desirable feature:
intuitively, if the data does not fit in the memory of a single machine, then
the application should be able to process it using multiple nodes of a cluster.
At very large scale, data analytics applications need also to provide provide
high-performance because, due to the scale and the nature of the problem to be
solved, the difference in performance draws the line between what is achievable
and what is unfeasible.

A typical approach to tackle these challenges is to highly customize a specific
application implementation to target a specific machine or architecture. Even
more aggressively, solutions based on hardware-software co-design tailor not
only the software implementation but also the actual target hardware, to solve a
specific class of problems.  Clearly, both of these approaches are characterized
by significant development/design effort, high time-to-solution, and low
portability/flexibility of both software and hardware.

In order to address these issues, we propose SHAD, the Scalable High Performance
Algorithms and Data Structures library.  SHAD is designed to achieve flexibility
and portability, while providing scalability and high performance.  Unlike other
high performance data analytics frameworks, SHAD can support a variety of
applications in several domains, including, but not limited to, graph
processing, machine learning, and, data mining.  The SHAD software stack is
composed of three main layers: an Abstract Runtime API, a collection of general
purpose algorithms and data structures, and, domain specific libraries, called
\emph{SHAD extensions}.  The Abstract Runtime API exposes a set of primitives
for managing tasks execution and concurrency and data-movements on both clusters
and single-node machine, hiding the low level details of the underlying runtime
systems and architectures.  On top of the AR API, we define the main SHAD data
structures layer, which includes general purpose data structures with C++
STL-like interfaces.  The core SHAD data-structures are designed to be
composable so that they can be used as building blocks for SHAD extensions.
This unique feature makes SHAD an evolving framework supporting of data
analytics applications.  In this paper we describe the design of SHAD, detailing
features and adopted solutions for its main components.  The main contributions
of our work are:
\begin{itemize}
\item the design of a general data-structure template, which can be adopted to
  implement data-structures on distributed systems.  Using this template, we
  have implemented the core data strucuters of the library and a graph library
  extension.
\item the definition of an abstract runtime API thad defines the minimal
  set of features of the underlying systems required by SHAD;
\item a performance and scalability evaluation of the general purpose data
  structures, and, of example applications implemented using the graph
  libraries.
\end{itemize}

The remainder of the paper proceeds as follows: \Cref{sec:methodology} overviews
the proposed methodology and design; more in detail:
\Cref{sec:methodology:runtime} defines the abstraction of underlying machine and
defines the minimal set of features required by framework;
\Cref{sec:methodology:ds-template} describes the general methodology used to
implement the data structure in the framework;
\Cref{sec:methodology:core-library} presents the core data structures;
\Cref{sec:methodology:graph-extension} presents an implementation of a graph
library using the data structure in the core library.  \Cref{sec:evaluation}
validates the proposed approach through performance and scalability studies on
the core library and the proposed graph extension.  \Cref{sec:relatedwork}
overviews related work. \Cref{sec:conclusion} concludes the paper and
anticipates future work and the path to publication.
