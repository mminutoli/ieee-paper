\section{Methodology}
\label{sec:methodology}

SHAD adopts a shared-memory programming model, even though data can be actually
spread over different nodes of a distributed system.  SHAD is designed as a
software stack, composed of three main layers: the Abstract Runtime Interface, a
library of core general purpose data-structures, and, SHAD extensions, which are
domain specific libraries obtained by composing SHAD data-structures and
possibly SHAD' extensions.

\subsection{The Abstract Runtime Interface}
\label{sec:methodology:runtime}

The Abstract Runtime Interface is designed to achieve portability by decoupling
the upper layers of the stack hiding the idiosyncratic details of the different
supported platforms.  We firstly explored the adoption of a runtime abstraction
layer with similar objectives in \cite{DBLP:conf/ipps/Castellana17}.

Starting from the experience of our previous work, we preserve the concept of
\emph{machine abstraction}, which models the underlying hardware platform as a
set of \emph{localities}.  A locality can be defined as a processing element
with directly accessible memory.  According to the peculiarities of the
underlying system, localities can model cores, NUMA domains, or nodes in
distributed systems.

In SHAD, we extensively use remote and asynchronous execution. Remote execution
is used to avoid possibly expensive data movements across multiple localities,
and thus reducing the network traffic.  Asynchronous execution is used to
exploit parallelism, and, in case of asynchronous remote execution, to tolerate
the communication latency of the network.  In fact, it has been shown that
asynchronous execution can help to speed up convergence for linear
systems\cite{DBLP:journals/ijhpca/Nagumey89}, speed up belief propagation
\cite{DBLP:journals/jmlr/GonzalezLG09}, and stocastic optimization
\cite{macready1996criticality}.

The abstract Runtime Interface supports asynchronous execution associating
identifiers, called \emph{handles}, to group related tasks and to check for the
termination of the asynchronous operations associated with it.  To achieve
high-performance, SHAD makes extensive use of asynchronous (non-blocking)
parallel execution, and remote task execution.  In particular, the
data-structure access methods allow to automatically move the computation where
the data is, rather than moving the data.  This is particularly beneficial when
the same operations (e.g. a function) is applied to multiple data.  A typical
example is visiting all the nodes/edges of a graph, or updating all the elements
of an array.

The main methods offered by the abstract runtime API are:
\begin{description}
\item[{[async]ExecuteAt}] [asynchronously] execute a function on a given locality;
\item[{[async]ExecuteAtWithRet}] [asynchronously] execute a function with a
  return value on a given locality;
\item[{[async]ExecuteOnAll}] [asynchronously] execute a function on all
  localities;
\item[{[async]ForEachAt}] [asynchronously] execute a parallel loop on a given
  locality;
\item[{[async]ForEachOnAll}] [asynchronously] execute a parallel loop on all
  localities;
\item[waitForCompletion] wait for the completion of asynchronous tasks.
\end{description}

From a programming model perspective, the Abstract Runtime Interface support
both fork-join and active messages.

\subsection{Generic Data Structure Template}
\label{sec:methodology:ds-template}

\begin{figure}[!t]
\centering
\includegraphics[width=\columnwidth]{../img/ads.eps}
\caption{\label{fig:methodology:ads}Abstract Data Structure Template}
\end{figure}

All the SHAD data structures implements distributed global objects that enable
give the user a shared memory abstraction on a distributed memory machine. The
data structures included in the framework follow the design pattern in
\Cref{fig:methodology:ads}.  Each data structure can be organized in three main
components: an object catalog, the data structure interface and the local data
storage.

The object catalog manages the object life cycle.  When a new distributed object
is created, the catalog generate a unique identifier that is associated with the
newly created instance.  Once the catalog has assigned an ID to the new object,
it sends an active message to all the other localities in the system to create
the local portion of the data structure that will reside on them.  To avoid the
complexity of synchronizing the locality in the system to assign identifiers to
new objects, the space of the valid object identifiers is partitioned between
the available localities.  Consequently, each locality has a limited but very
large number of object that can be concurrently instantiated for each type
($2^{48}$ in the current implementation).  The assigned object identifier will
act as a global pointer to the instantiated global object across the system so
that, when the computation moves from one locality to another, it will be used
to get access to the local portion of the distributed object.

The data structure interface defines the behavior of the data structure and
provides a global view on the accessed object.  Each interface provides two
classes of operations: access/modifiers methods and visits.  All the methods are
usually provided in a synchronous and an asynchronous mode of operation.  The
visit methods accepts as input a user defined call-back function that will be
used to operate on the data at the locality where it is stored.  The underlying
implementation of the data structure interface will use the catalog and an
aggregation buffer to move computation and data between different localities
using the services provided by the abstract runtime interface.

The last component of the design pattern is the local data storage.  The design
pattern that we have developed does not make any specific assumption on the kind
of storage for the data.  In the current implementation, the SHAD data
structures uses the main memory to provide high performance.  However, we are
planning to extend the local data storage to exploit persistent storage to
support mechanism for relaibility.

\subsection{The core data structure library}
\label{sec:methodology:core-library}

SHAD's core library implements four common data structures: \emph{Array, Vector,
  Hashmap, Set}.  All the core data structures are designed to be thread safe
and to be composed to build application specific extensions and
\Cref{sec:methodology:graph-extension} will detail on how to use the
\emph{Array} and the \emph{Hashmap} to implement a graph library.

The \emph{Array} provides a C++ like distributed global array.  Conformingly
with the C/C++ array, the SHAD \emph{Array} is of fixed size which is defined
when the object is created.  The current implementation spreads data evenly
across the system in order to maximize data locality when accessing contiguous
blocks.  However, different distribution policies are under development to
provide an extra degree of freedom for applications that needs it.

The \emph{Vector} data structure provides a dynamically expandable array.  In
order to efficiently support the dynamic expansion, the data is striped across
the system in fixed size blocks.  This data layout enables the dynamic expansion
of container and support operations like \emph{push back} of new data and
\emph{insert} of sequences beyond the current size of the container.  However,
the insert operation provided from the SHAD \emph{Vector} has a different
semantic with respect to the standard insert method provided in the STL vector
for performance reason.  In fact, the standard behavior prescribes that all the
data beyond the insertion point must be move to make space for the newly
inserted data.  While this is feasible when managing small amount of data on a
single node system, this is unacceptable on big data applications and on
distributed system that are the main target of the SHAD library.

The \emph{Hashmap} provides a distributed hashmap that supports concurrent
insertions and deletion.  Its current implementation uses hashing at two
different levels: the first will uniquely identify the \emph{locality} where the
data will reside and the second level identifies the position in the local
storage.  The Hashmap interface provides operation to visit all the key/value
pair and to operate on all the key.  In SHAD, the \emph{Set} is a specialized
version of the \emph{Hashmap} where no value is stored for each of the key
inserted.

\subsection{Graph Extension}
\label{sec:methodology:graph-extension}

