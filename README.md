# IEEE paper project.

This repository contains a template project to be used for IEEE
conferences and jurnals.

## Build instruction

You should have _*biber*_ installed on your computer.

To build the paper simply run the command:

    waf configure
    waf build

If waf complains about missing tools, read the message and follow the
instructions.
